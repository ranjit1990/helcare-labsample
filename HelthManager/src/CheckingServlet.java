

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CheckingServlet
 */
@WebServlet("/CheckingServlet")
public class CheckingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	
	response.setContentType("text/html");
	
	PrintWriter output = response.getWriter();
	
	String name = request.getParameter("Name");
	System.out.println("Name "+name);
	String gender = request.getParameter("Gender");
	System.out.println("Gender "+gender);
	String age = request.getParameter("age");
	int agee = Integer.parseInt(age);
	
	System.out.println("age "+age);
	
	String healthParameter = request.getParameter("Current_health");

	String habbitParameter = request.getParameter("Habits");
	float Basepremium=0;
	int percent=0;
	Basepremium=50000;
	float total=Basepremium;
	if(agee!=0){
		
		
		if(agee>18)
		{
			int temp=25;
			while(agee>temp)
		
			{
				percent+=10;
				temp+=5;
			}
			if(gender.equals("Male"))
			{
				percent+=2;
			}
			if(!healthParameter.equals("NO"))
			{
				percent+=1;
				
			}
			if(habbitParameter.equals("Daily_exercise"))
			{
				percent-=3;
			}
			else{
				percent+=3;
			}
			System.out.println("percent "+percent);
			float per=(percent*Basepremium)/100;
			System.out.println("per "+per);
			total=Basepremium+per;
			System.out.println("total "+total);
			
		}
		System.out.println("Health Insurance Premium for "+ name+": Rs."+ total);
		output.write("Health Insurance Premium for "+ name+": Rs."+ total);
		String resp="Health Insurance Premium for "+ name+": Rs."+ total;
		 request.setAttribute("resp", resp);
		response.sendRedirect("success.jsp");
	}
	
	

	else{
		response.sendRedirect("Failure.jsp");
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
